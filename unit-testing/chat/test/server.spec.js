const server = require('../server.js');
const request = require('request-promise');
const config = require('../config/index.js');
const fs = require('fs-extra');
const path = require('path');
const assert = require('assert');
const { COPYFILE_EXCL } = fs.constants;
const faker = require('faker');


	// Написать тесты на
	// get запрос к http://localhost:3333  => вернет index.html
	// get запрос к http://localhost:3333/file.txt => вернет file.txt

describe('GET request', () => {
	let app;
	const URI = 'http://localhost:3333'
	before((done) => {
		app = server.listen(3333, () => {
			done();
		});
	});
	
	describe('index.html', () => {
		const indexFile = path.join(config.publicRoot, 'index.html');
		let indexHtml;

		before(() => {
			indexHtml = fs.readFileSync(indexFile, {encoding: 'utf-8'});
		})

		it('should be index.html', async () => {
			const res = await request({
				method: "GET",
				uri: URI,
			});
			assert.equal(res, indexHtml);
		});
	})

	describe('file.txt', () => {
		const filePath = path.join(config.filesRoot, 'file.txt');
		const fileContent = faker.lorem.text();

		before(() => {
			fs.writeFile(filePath, fileContent);
		});

		it('should be file.txt', async () => {
			const res = await request({
				method: "GET",
				uri: URI+"/file.txt",
			});
			assert.equal(res, fileContent);
		});

		after(() => {
			fs.removeSync(filePath);
		});
	})

	after((done) => {
		app.close(() => {
		done();
		});
	});

});
